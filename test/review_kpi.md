# Review of Success KPIs
KPIs defined in the solution design phase need to be tested with code and user testing results. Some KPIs such as economic performance of a software solution may need other data which may take time to collect. This document outlines the way in which KPIs are documented and how to typically rate and measure success.

Jira Issue:[DIG-37](https://arupdigital.atlassian.net/browse/DIG-37)