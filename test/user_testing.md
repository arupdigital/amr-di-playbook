User Testing
=============
This document specifies the process for user testing and is tied directly to the user experience design process used in the solution design phase. [DIG-36](https://arupdigital.atlassian.net/browse/DIG-36)


[TOC]

Supervised "Controlled" Usability Testing
-----------------------------------------------------
_Last Updated by Andrew Nguyen - September 2019_

### The General Rundown

A supervised "controlled" usability test is generally one that involves one or more observers moderating a session in which an individual will go through a working build of an application of interest. This session would ideally be structured with participants going over predefined feature-related tasks while speaking their thoughts aloud. This type of session can be recorded or broadcast for other members of the development team to return to or learn from, ideally by capturing the on-screen interactions as well as the user's voiced thoughts. The goal of this exercise is to get an understanding of how users will be going through the application and to gather any information on bugs, logical issues, or design flaws that may not have been considered by the development team in isolation. 

### Putting the "User" into User Testing

User testing should generally be kept as simple as possible, allowing for the user to go through the application without being distracted. The first step is to find people to test with. A good place to start looking are the project pages on the Arup intranet if your project/application is tied to a certain Arup Project (i.e. DELTA LAX). It may even be a good idea to run some sessions with developers to see if they encounter any problems/issues that they may not have noticed before in code review. If you have any direct clients that are contactable or know of anyone that would eventually be using the application they should ideally be the first person you go to for user testing. After user testing with one person it should become easier to find more testers by asking your participants who they might recommend to take a look at the app. Overall you should have between 5-7 participants for user testing per large feature release as that [has been shown](https://www.nngroup.com/articles/why-you-only-need-to-test-with-5-users/) to be the optimal number for gathering insights before they become repetitive.

### Seeking a Space 
 Once you've found a participant, you'll need to determine where the testing should occur. This will generally depend on the context you expect the app to be used in as well as the resources available. For example, if you expect the test participant to be using the application on-site it might be a good idea to go on-site with them on the other hand if you expect them to be using the application in the office it may be good to have them go through the app at their own desk (See Field Studies).  

 Unfortunately, testing on-location does come with certain consequences. This could come in the form of difficulty recording audio at a loud construction site or having to deal with distractions and onlooking observers at someone's desk. So unless getting on-location use cases is your focus, running a test in one of Arup's many wonderful conference rooms should suffice (preferably one with conferencing capabilities to take advantage of recording devices). As such, this section in particular will primarily outline a "controlled" type of study. 

 *Note: This sort of user-testing should generally take around 1 hour. Make sure that both you as well as your participant have enough time allotted to the session.

 Once you have a place/setting in mind along with your list of potential subjects it's time for you to start the real planning.

### Perfect Planning 

When it comes to usability testing having a plan should be your first priority. Without a good plan you end up wasting not only the project's time but the time of your volunteering participants. Going into your session you should have:

1. A list of tasks you want your participants to complete on the app you are testing. These tasks should be as low-level as possible while still remaining relevant with what you want your app to be used for. For example, if you are trying to test a dashboard application a good task to have users run through may be to have them find certain important pieces of information or having them input some generic information into the dashboard from their own device.
    * When your tasks are being done by your participants, have them speak their thoughts aloud. This is a good way of understanding their processes and finding any discrepencies between what they say and what they do.
    * Sometimes, if your app is in a state where certain tasks are accomplishable but very difficult to do it may be helpful to include a few hints to give to participants in the case that they get stuck somewhere. 
    * It is _**very**_ important that you ensure that all of your planned tasks can be accomplished on your app at most a day before your session. Nothing is worse than having your participants get roadblocked on a task all because a bug was introduced in the latest build. It makes your app look unreliable and also prevents you from getting as much usable data as possible from that task.

2. A set of questions you may want to ask your participants between each task. These should be more general questions or ones that apply specifically to the task they may have just completed. (i.e. Did that feature behave in the way you expected? or Is creating a markup something you would normally do as part of your work?).

3. A set of questions you may want to ask your participants after all of their tasks are complete. These could be questions that apply to the app as a whole or require participants to compare between them. (i.e Are there any features that you feel are missing from the app? or Which of these tasks did you find most difficult to accomplish?)

4. You may also want to have a list of preliminary questions if having a background understanding of your test participants plays a role in how they might use your app. (i.e. What discipline are you from? or Do you primarily work from the office or on-site?)

5. If you have a set of interactive mockups on Figma, Sketch, InVision, or any one of the dozens of other mockup tools out there, it may also be a good idea to have a sepearte list of tasks and questions made for those mockups based on the features that have been prototyped. This is a good chance to showcase that some of the missing features your users may have mentioned during the app test have already been considered or just a fine opportunity to see if the mocked-up features behave in the way your users expect them to.

It is important to note that your plan may not be the exact same between each and every one of your sessions over a given time span. Tasks and questions can be changed based on observations/feedback from prior sessions or even just based on the background and knowledge of the participant themselves. 


### Starting Your Session 
Before your session begins try to give yourself at least 10 minutes of time to prepare the room/materials for the testing session. Set up any and all recording devices and make sure that their output is as clear as possible. The amount of equipment you may use will vary but at the very minimum the screen as well as any speech/audio should be captured. If there are any people interested in participating in the session, with the right tools it could be broadcasted via Skype as to not fill the room with observers. A third person view can also be beneficial if you're interested in facial expressions or interactions not captured on screen. Fancier equipment can be used as well such as eyetracking or even a Kinect. More data doesn't always mean better results, however, as having more recording methods can get distracting for your participants and makes it harder to organize your own thoughts/observations. Once everything is set up, ensure that the app is running on your own device just in case and be sure to remind your participant to bring their own device if possible. Make sure that you have your list of tasks placed somewhere such that it can be seen by your participants and have a notebook/text file available to note down any on-the-spot observations.

### A Session Scenario
If you planned your user testing session beforehand, the session itself should run relatively smoothly. Following the outlined planning above, your study may proceed as follows (there may be differences depending on how/what you planned for the study):

1. Ask any preliminary questions regarding the participant's background and prior knowledge.

2. Have the participant go through an outlined task, speaking their thought process aloud and asking for help if they feel they are stuck. At this time you should be writing down and interesting observations such as discrepencies between their thoughts and actions, feedback/criticisms, or any bugs/issues with the app itself.

3. Once the participant completes their task ask them any between-task questions and note down any interesting observations. If you have any questions about a certain action they performed or thought they spoke aloud, now would be the best time to enquire about it. (Why did you that in the way you did? or You said X but ended up doing Y, was there a reason for that?) 

4. Repeat steps 2 and 3 for all of the tasks you had planned within the app.

5. Ask any post-task questions you have planned and use this opportunity to clear up anything confusing that might have come up during the task runthroughs. This is a good time to ask for recommendations for any future participants.

6. If time permits, it may be a good idea to bring up any mockups/prototypes and either have participants look through them freely or go through any tasks you may have prepared for them. If the participant brought up any features they may have wanted but were missing from the app, it may be a good idea to show them any mockups related to said feature (if they exist) to ask for relevant feedback.

As you are running through the session, keep in mind that time is valuable and try not to spend too much time focusing on a single task. Try to keep any questions you have relevant to the task at hand and ask any other questions afterwards once all of the tasks have been completed. This ensures that the user has time to go through and give data for all of the features you want them to see.

### Compiling Your Results 
As you go about documenting the observations from your sessions it is important to do so in an organized fashion such that the context of the observations aren't lost. There could be a number of ways to do this, but one way that generally seems to work is to organize observations by the tasks in which they occured. 

Once you have gathered observations over a set period of time it may be a good idea to bring them all together and package them into a presentable form. This could be done, for example, as a spreadsheet of issues that were previously undiscovered, already addressed in production, or to be discussed further with the developer team or client. There are many other ways besides this to package your results but it will all depend on how your observations will be used. As you are bringing everything together, try to go back to any recordings you've made and make sure that every notable observation has been properly documented and reflected in your presentation. 


### Some Helpful Resources

1. [The Nielsen-Norman Group Website](https://www.nngroup.com/), which hosts numerous articles and videos from leading experts on design research, is a great starting point for learning more.
2. [Usability.gov](https://www.usability.gov/how-to-and-tools/methods/index.html) is also a great resource for learning about all the types of design and design research methodologies out there.
3. [Medium's User Research section](https://medium.com/user-research) is also a great place to find insights from people within the field of design research although keep in mind that many of the articles are self-published which means that some articles will be written by industry experts while others by amateur design research enthusiasts. Both of which can have valid opinions but it will depend on what you are looking for. It should also be noted that some articles may also be locked behind a paywall.

Other Testing Methods Not Yet Discussed 
---------------------------------------------------------

1. A/B Testing
2. Unsupervised Usability Testing
3. Heuristic Evaluation
4. Remote Usability Testing
5. Field Studies
6. Card Sorting
7. Focus Group Study

