# Agile Meeting Notes
This document specifies the meeting note formats of various agile meetings from scrums to sprint reviews to client update meetings during the development phase of a project. Client update meetings should also be covered in the project management tools document.

Jira Issue:[DIG-32](https://arupdigital.atlassian.net/browse/DIG-32)