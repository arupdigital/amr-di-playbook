# Development Quality and Process Control
This document outlines a mid project review process, similar to a retrospective or architecture review, but focuses on the project's adherence to the Playbook, and outlines a process for auditing a project, documenting challenges, and recommendations for improvement.

Jira Issue:[DIG-30](https://arupdigital.atlassian.net/browse/DIG-30)