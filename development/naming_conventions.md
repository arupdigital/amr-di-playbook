# Naming Conventions, Project Folder Structures, and Repos
This document specifies the proper project naming convention, folder structure, MIcrosoft Teams folder and chat structure, and more. Any Arup network project structures should follow Ken Goldup's project folder structure for all north america engineering projects.

Jira Issue:[DIG-33](https://arupdigital.atlassian.net/browse/DIG-33)