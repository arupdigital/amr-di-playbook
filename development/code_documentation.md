# Code Documentation
This document addresses how to document the code we write so that intent and functionality is clear to anyone working on the project, at various skill levels.

## Why Document Code
Have you ever picked up a project and had no idea how to set it up? Or had to fix code that was written by someone who left the firm? Or looked at a function and not known what it's doing? This is why we document code. We can spend days or weeks trying to answer questions that could have been addressed with a couple comments. 

In an ideal world, our code should be so good and well-written that we don't need to explain it. But we want what we make to be usable by other people, and no matter how good we think our code is, it's useful to document functionality and intent. This goes hand-in-hand with writing code that is clear and well-structured; no amount of commenting can save you from poorly written code. This topic is covered in a different section. 

There are two main audiences for code: users and maintainers. Users are concerned with how to _use_ the code, while maintainers are concerned with what the code _does_ and _how_ it works. We should be aware of which parts of our documentation are for which audience and address their needs.

## General Guidance
Code documentation should always put the reader first and be written for humans. Brevity at the expense of clarity will only cause problems later on. There are a few general guidelines you should follow when documenting your code:

- Write your documentation like you'd explain the code to a friend. Remember, we're writing for people. Write complete sentences, be clear, concise, and understandable with your language, and limit your jargon to the bare minimum.
- Documentation should answer real questions. It's not useful to label a hammer "hammer." Documentation should address what the intent of something is (to affix two pieces of wood), what it does (it drives nails), and how it works (you hit stuff with it). 
    * Not all of these questions are necessarily best answered in the same place. Put documentation where it's most useful to someone reading it. Keep comments close to the code they refer to. 
- Code should be clear and use easy to understand concepts; code should try to comment itself. Using clear concepts goes a long way to making your code understandable so that it doesn't need comments. 
    * Type hints, descriptive names, standard functions, and more help make code understandable.
    * In this example, see how writing code to be general, using descriptive names, and using type hints results in clearer, better code with no comments necessary. 
       
        Bad:
```python3
    # Calculate 3-D distance from the origin to some point in space
    def func(a, b, c):
        if type(a) == float and type(c) == float and type(d) == float:
            # The distance is the square root of the sum of the squares of coordinate
            d = math.sqrt(a**2 + b**2 + c**2)
            # return the distance rounded to the nearest hundreth
        else:
            print('arguments must be of type "float"')
            d = 0
        return round(d, 2)
```
        
        Good:
```python3
        def calculate_distance(x_0:float, x_1: float, y_0: float, y_1: float, z_0: float, z_1: float, precision: int = 2) -> float:
            distance = math.sqrt((x_1-x_0)**2 + (y_1 - y_0)**2 + (z_1 - z_0)**2)
            return round(distance, precision)
```
     
- Comments should provide explanation when there are nuances in environment or assumptions in the code. Where there are difficult or subtle pieces of code, always discuss what's going on.
    * Don't include redundant information in your comments. 
    
        Bad:
```python3
    # Main function to run the code
    def main(argv):
        # Read the model.csv file from output folder into a pandas dataframe
        root = argv[1]
        output = root + 'Output\\'
        df = pd.read_csv(output + 'model.csv')
        print df.head()
```

> Code tells you how; Comments tell you why.
>
> — Jeff Atwood (aka Coding Horror)



### README.md
Every project needs a README, whether it's a full-stack application of a couple of scripts. This should have all of the information someone needs to get started with your project. That probably includes:

- Project explanation
- Tech & frameworks used
- Code style
- Setup instructions
- Contribution guide
- How to use the project
- Relevant licenses, credits, links... Whatever would be useful! 

README files should be properly formatted using Markdown syntax. Resources for Markdown are linked at the end of this document. 

This is the face of your application and the starting point for anyone working on it. In the case of open source code, the README should include additional detail for contributors, build status, demos, and other useful resources. 

#### Project Overview
The project overview is an often omitted section of a README that provides key information to someone getting started on a project. This section should explain not only what the project is, but the motivation behind the project. This is incredibly useful to someone getting started so they understand the _intent_ of the code. This section should address what problem the project is solving and how it goes about solving it. Diagrams, workflow maps, gifs, or other media can be included here to help convey the 

#### Project Setup
This section should address the exact steps to get the project running locally on the developer's machine. Setup issues are rampant in software, so it's imperative to make this section detailed and thorough. Depending on the application architecture, this can be relatively straightforward or deeply involved. This section should be able  to guide a developer through a fresh setup of the project, meaning there can be no assumptions about existing environment conditions, installed applications, dependencies, or tools -  you're starting from scratch.

Finally, the steps to run the application locally, run tests, linting, builds, or whatever else should be covered here using `code formatting` wherever applicable. 


#### User Guide
This section should explain how the application is used in practice. This is useful for both end users and developers, as it shows what the project actually does and what functionality it has. Clickthrough videos, screenshots, and other media are also useful here to show the app in action. 

### Dependencies
Dependencies should be fully documented in language-standard ways. Developers should not be left guessing which versions of software they need to use.Typically, dependencies are documented in `package.json` in Javascript applications and `requirements.txt` for Python applications. This provides a replicable way of installing dependencies for developers. 

Docker is another tool to address this problem. We won't address Docker in detail here, but encourage you to use it. 

## Python
In addition to README files, Python code should be properly commented and make use of docstrings on function and class definitions. Comments in Python use the `#` symbol or triple quotes (`""" ~comment~ """`).

### Docstrings
The official Python docstring convention is linked in the References below. A docstring, according to the spec is:

> a string literal that occurs as the first statement in a module, function, class, or method definition. Such a docstring becomes the __doc__ special attribute of that object.

It is normal convention to use docstrings on all functions, classes, and modules. Docstrings used in functions should outline the function arguments, the expected returned value, and provide an explanation of the function logic. An example function using a docstring docstring is below:

```python3
def parse_results(result):
    """
    Process results from eplusplus

    :param result: A set of results for one individual
    :return: Clean results or None
    """
    if result is not None:
        energy_usages = eplusplus.parse_energy_results(result)
        return energy_usages
    else:
        return None
```

### Inline Comments
Inline comments in Python should be used to briefly explain what the code is doing or to expand on pieces of code that require additional explanation, clarification, or instruction. Examples of some code comments in Python code are below:

```python3
if __name__ == "__main__":
    # Turn these on only for post-processing tasks
    creator.create("MinEnergyMinCost", base.Fitness, weights=(-1.0, -1.0))  # Update based on number of objectives
    creator.create("Individual", list, fitness=creator.MinEnergyMinCost, profile=dict)  # Create the individuals
```

Functionality to be added, code to be fixed, or other to-dos should be documented using the `# TODO:` syntax.

## Javascript
Write comments in Javascript using `//` or block comments using `/*` to open and `*/` to close a comment block.
### JSDoc

### Inline Comments
Inline comments in Javascript should be used to briefly explain what the code is doing or to expand on pieces of code that require additional explanation, clarification, or instruction. Examples of some code comments in Javascript code are below:

```javascript 1.8
/**
* Listen for when the mouse leaves the map element and remove all highlights
*/
registerLeaveListener() {
...
}


// pass all changes to child nodes
this.childNodes.forEach((node: any) => {
...
});

```

Functionality to be added, code to be fixed, or other to-dos should be documented using the `// TODO:` syntax.


## API Documentation
Regardless of the technology, it's useful to document API endpoints and sample responses. For each endpoint, document the request and response. Full responses aren't necessary as they can add bloat to the README - short snippets are fine. 

If the application is using serverless functions as an API (such as AWS Lambda and API Gateway), include the urls for those endpoints and the links to the AWS resources. 

## Deployment
The deployment of the application is another important area to document properly. The location of resources, pipelines, environment variables, and other deployment configuration should be documented so that someone could replicate the deployment. 

Step-by-step instructions to set up a deployed version of the application (internal and/or external) should be provided going over how to deploy the app from scratch. 

## Maintaining Comments and Documentation
When we refactor code, we need to refactor comments. Old comments can be more confusing than no comments. Whenever we rewrite code or make changes, be sure to update your documentation and comments to reflect those changes. _This should not be treated as a nice-to-have._ Our documentation is just as much a part of our source code as the algorithms and application code that we spend most of our time writing, so we should maintain our documentation just like we maintain our "normal" code.

Many IDEs provide linting and hints where docstrings don't match function arguments, highlighting for todo's, and more, so use those tools to help keep our code updated. 

README files, architecture diagrams, and other resources should be properly maintained as the project goes on. 

## References
[How to document source code responsibly](https://medium.com/@andrewgoldis/how-to-document-source-code-responsibly-2b2f303aa525) - Andrew Goldis

[Markdown Syntax](https://www.markdownguide.org/basic-syntax/)

[Python Docstrings Spec](https://www.python.org/dev/peps/pep-0257/)

[Documenting Python Code](https://realpython.com/documenting-python-code/#commenting-vs-documenting-code) - James Mertz