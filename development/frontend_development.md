# Frontend Development
This task can cover guidelines or tips to share when building web front-end.

The topic can be broad, for example, about existing front-end framework and how to testing and debugging.

Or, specific tips like Andy's idea, how to build skeleton of sites (e.g. using Angular CLI) avoiding custom made skeleton.

Jira Issue:[DIG-68](https://arupdigital.atlassian.net/browse/DIG-68)

## Choosing a Framework
There are dozens of open-source frontend frameworks out there, but we typically work in two (or maybe three) of the most popular. Choosing a framework for your project involves a few different considerations. Most importantly is skills on your team and the ease of use of the framework. If your project has developers with strong React skills, it probably makes more sense to use React than to learn a new framework from scratch. It's also worth considering library support. If your project requires specific functionality from a library, it's worth considering if that library is well supported in your framework. Some other things to consider are:

- Developer tools
- Community support
- Scalability
- TypeScript support

One argument often had on the internet is about differences in performance between frameworks. Certainly there are differences, but in the _vast majority_ of cases, this doesn't matter. Choose your framework based on ease of use and how it will work on your project. 

### Angular

[https://angular.io/](https://angular.io/)

Angular is one Google's frontend framework and one of the more popular frameworks. Angular extends html markup and provides a lot of built-in functionality. 

Angular is the preferred framework for most projects in the Insight team for a few reasons. First and foremost, Angular is an _opinionated_ framework. Angular apps should all follow the same structure and principles, so an app on one project should have a similar structure to another Angular app. Angular is a "batteries included" solution, provides a lot of the functionality we need for a robust site built in: routing, testing, animations, Material Design components, progressive web apps, universal, accessibility, etc. Angular also uses TypeScript, which allows strong typing on JavaScript code. 

The most powerful part of Angular is the CLI (command line interface), which allows you to scaffold an entire project in one command, generate components, manage packages, and more.

`npm install -g @angular/cli` installs the Angular CLI globally.

`ng new my-app --enableIvy --style=scss` initializes a new Angular app using the shiny new Ivy rendering engine and scss files. 

`ng add @angular/material` adds the Material library.

`ng add @angular/pwa` adds Progressive Web App functionality.

The CLI removes a lot of the boilerplate code that we need to write and lets us get an app up and running very quickly. Adding packages using the `ng add` command handles the wiring and imports to the application as well. 

A very useful feature of Angular are "schematics." The `ng generate` command can be used to generate everything from components to services to guards - anything that an Angular app uses. Material schematics in particular are very useful when first scaffolding an application. 

`ng generate component my-component` creates a component with the valid html, TypeScript, css, and tests. 

`ng generate @angular/material:address-form <component-name>` creates an address form component with Material Design components. 

Angular also has built-in support for the Material Design spec, which is the most widely used and best-maintained UI framework on the web. Material components are common across all of Insight's applications. 

Coding using the Angular framework means coding in an "Angular" way. Angular revolves around components, services, and modules, among others. A typical Angular component has three parts: the html markup where the interface is defined, the css (or scss, less, etc) that contains styles for the component, and the TypeScript file that contains the component definition and TypeScript methods that relate to the component. The html and TypeScript are linked through "bindings," which link data in TypeScript to some part of the html code. Methods like if-statements, for loops, and dynamic behavior can all be defined though Angular tags in html referencing TypeScript Objects. Binding is one of the most powerful parts of the Angular framework, as it can bind a value in TypeScript to html and automatically update that value as it's changed by the user. 


### React
[https://reactjs.org/](https://reactjs.org/)

React is a UI library built by Facebook. React differs from Angular in that it isn't a full framework - it's a tool for building user interface components.

Coding in React is coding in JavaScript. React apps use JSX (JavaScript XML) to define html markup as code. Using JSX, we can define components to render using React. Therefore, using React requires some strong Javascript skills. 

JSX Code:
```javascript 1.8
const element = <h1>Hello World!</h1>
```

Like Angular, React has some methods to help with application creation, if not to the extent that Angular does. 

`npm create-react-app my-app`

It's up to the user to determine how they want to implement functionality in React apps. Routing, Flux, animation, UI libraries, and more all are set up by the user. Luckily, there is a robust ecosystem of packages to choose from. React gives more choice and freedom to the developer than Angular by sacrificing some convenience. 

### Vue
[https://vuejs.org/](https://vuejs.org/)

Vue is the least adopted framework that we'll look at, but is still quite popular among developers because it is the most lightweight of these three. Vue has some similarities to both Angular and React in how code is written. Like React, Vue is not a fully-fledged framework - it's a UI library. It's still up to users to implement routing, UI libraries, etc. 

Vue.js templates are primarily valid html markup. A `.vue` file contains html in a `<template>` tag that will be rendered, a `<style>` tag that holds the component's styles, and a `<script>` tag that holds the JavaScript data, methods, imports, exports, etc. This architecture makes Vue quite easy to get started with for new developers.

Like the other frameworks, Vue can use a CLI to help with development and scaffolding an initial project.

`vue create my-project`

## Flux Patterns
The [Flux](https://github.com/facebook/flux) architecture is gaining popularity among frontend developers and bears some discussion. Flux is a pattern for data management in an application that revolves around a store, actions, and dispatchers (and effects, reducers, selectors...). This pattern is popularized by Facebook for their React framework, which uses the Redux library. There are hundreds of explainers on the internet, but as a quick primer, Flux apps use a store to manage application state. Components get data from the store and send "actions" to "dispatchers," which then update the store. The pattern utilizes a "one-way data flow" and an immutable store as the means of managing data flow and application state. 

In React, the Redux library adds Flux architecture, while in Angular, ngrx is the preferred Flux library. 

Flux patterns allow for step-by-step logging in the store as values change, so you can see the values before and after actions are taken. Using this same structure, you can "rewind" to past states. 

### When to use Flux 
It should be noted that implementing Flux architecture on an app adds quite a bit of overhead and learning to a project. When we add one of these libraries, every new piece of data in the store requires a reducer, actions, dispatchers, selectors, effects, and all of the logic to ties those together. We find ourselves writing a lot more boilerplate code when using these libraries. This can make it difficult to follow the logic from a button press to updating the store, especially for new developers on the project. 

Flux apps can quickly spiral in complexity, so we should be deliberate about when to add Flux and which parts of the application need to be integrated with the store. When deciding whether to add Flux patterns to your project, consider:

- Are we sharing the same data between many components?
- Does that data change as the user interacts with the page?
- Does the history of changes in the data matter?

> A shopping cart on an e-commerce site is a good use case for Flux architecture. As the user navigates around, we want to store what they put in their cart in a simple way.

Not everything needs to go in the store! In fact, storing all of your data in the store can add unnecessary complexity to your app. We should be deliberate about what we put in the store and how we update the store to keep data flow clear and simple. 
