# Project Management Tools
This is a document which outlines the tools needed to manage a software project at Arup, on the Insight team during the development phases of a project. The document can then also specify particular processes, templates or methodologies used within each solution stipulated.

Jira Issue:[DIG-29](https://arupdigital.atlassian.net/browse/DIG-29)