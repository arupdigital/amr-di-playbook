# Development Considerations
This section contains information regarding high-level development practices ranging from naming conventions to security threat brainstorm sessions.

## Agile Development

* [Project Management Tool](project_management.md)

* [Agile Meeting Notes](meeting_notes.md)

* [Sprint Summary](sprint_summary.md)

* [Frontend Development](frontend_development.md)

* [Code Documentation](code_documentation.md)

* [Code Review](code_review.md)

* [Code Analysis](code_analysis.md)

* [Development Quality and Process Control](development_quality.md)

* [Deployment Execution](deployment_execution.md)

* [Security Threat Brainstorm](security_brainstorm.md)


