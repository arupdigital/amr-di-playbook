# Sprint Summary
This task documents sprints. A sprint sets out tasks with a specific deadline. There are different types of sprints, for example a design sprint, a code sprint, etc. This document summarizes sprints, outlines them, and when possible provides templates and methodology.

Jira Issue:[DIG-25](https://arupdigital.atlassian.net/browse/DIG-25)