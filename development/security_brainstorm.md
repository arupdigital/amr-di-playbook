# Security Threat Brainstorm
A document which outlines the process for undertaking a security threat analysis brainstorm with the outcome of a security plan, improvement plan or specific code changes. This can be during solution design and development so this document should be in both epics.

### Purpose
The purpose of this document is to outline a methodology for identifying threats in terms of who, why, and how. Another document should capture standard procedures and tools for mitigating those threats. The below methodology is common to intelligence organizations and is built on a concept of applying existing knowledge to the current situation. This seems elementary, but, in many ways, is the opposite of how we behave on a daily basis. Generally, we take in the environment around us and pull knowledge relevant to the situation to make decisions. This is perfectly fine for relatively simple situations, but it is well known that we are not as good at assessing our situation as we think we are [(Awareness Test)](https://www.youtube.com/watch?v=Ahg6qcgoay4). Therefore, we are also not very good at bringing all pertinent information into our problem solving process because our picture is incomplete. That is why a deliberate process is necessary to force us to set aside assumptions, mental schemas, and bias so that we can identify where we are vulnerable and who might exploit that vulnerability.

Following this method for every new project is not practical. However, using all previous security protocols into future projects is. In this way, our security can be cumulative rather than disjoint. Many of the same people will be trying to access our systems for the same reasons and with the same methods. Therefore, this methodology may be more appropriate for determining the security protocol that is the default for Arup projects rather than doing this on a case-by-case basis. It is also important to recognize when we enter an industry or project which exposes us to new threats. We also need to be up to date on current events so we can learn from others' misfortune rather than our own.

### Identifying Actors
This can be a particularly difficult part of the process. In a connected world, anybody could interact with the products we develop. Since we can't plan for everybody, we need to start by focusing our attention on those that would be interested in us. We can identify a few groups to help narrow our suspects. This is not an exhaustive list and it is not meant to indicate that every project we work on will have interest from all or even any of the below entities. While some of these may seem far fetched, the articles at the bottom of the page indicate these should be [real considerations](https://www.rand.org/content/dam/rand/pubs/testimonies/CT400/CT490/RAND_CT490.pdf).

- Nation-state Actors

- Terrorist Organizations

- Client Competitors

- Our Competitors

- Disgruntled Employees

- Regular Employees

- Regular Users

- Hactivists

- Hackers at large

### Identifying Motivations
Understanding what motivates these entities is similar to understanding what motivates our clients and their customers. We need to understand what they want and why. We generally don't get the opportunity to do user interviews, though, so we have to rely on research, imagination, and empathy. That research can be done specifically about a potential subject, or it can be staying up-to-date on current events in the cyber world and geopolitics. When identifying motives, we have to be careful not to deviate into identifying actions. Often specific motivations will result in specific actions, but that is not the purpose of this step. Some potential intentions:
1) Obtain data to exploit
1) Obtain data to sell
1) Obtain data to embarrass
1) Disrupt national services
1) Cripple critical infrastructure
1) Cast doubt on credibility of the client
1) Expose something they believe is wrong
1) Cast Arup in a bad light
1) Just cause general disruption
1) Another intention is that there is no intention of doing harm. But a regular Arup employee or user of the product could accidentally do something bad.

All of these *results* are things that can and have been accomplished in numerous ways in the past. The digital environment is just another attack surface that will be exploited if is weaker than other defenses and consumes fewer resources or exposes the attacker to less risk. If they want access to data and our locks are good, they'll take off the hinges. If our hinges and everything else about the physical security is good, they'll attack our digital infrastructure.

### Identifying Capabilities and Resources
We must analyze each entity individually. We need to think about where they are, what tools they have, what level of sophistication, and how much resources they have. Some key things to consider:
1) *Access*. For the specified entity, what do they have access to. Access consideration should go beyond the digital landscape. Some controls could be best implemented with a physical fix. The reason this is so important is because the interface between the digital world and physical one can be left ultra exposed because the teams working in those distinct environments are not aware of or considering effects to the other team. That puts the onus on the digital team to extend our considerations into their domain rather than the other way around.
1) *Tools and Methods*. What has each group used in the past. Do they get a worker to plug in a malicious USB, open a bad email, phish, whale, gain access via third-party etc.?
1) *Sophistication*. How good are they at the above? Is the sophistication something that can be fixed wtih a very low-cost solution or does it require a complex and well-orchestrated security plan?
1) *Resources*. How many people are involved? Is it a dedicated team or a singe individual? Are they working locally or distributed around the globe? How much money do they have available to fund their operations? For a lot of these entities, they are running a business. We don't need to block them from everything they might be able to do, we need to block them from doing what they will reasonably do. If our defenses are enough that their return on investment is low enough that they don't attack or they give up on the attack when they realize it's not worth it, then we have accomplished our goal. Time is a resource and we should consider how long it would take to mount such an attack.

### Predicting Actions
Like fiction writers, we need to write believable narratives that synthesize the above information. We have identified who we need to watch out for, we know why they might do it, and we know what means they have. Now we need to put ourselves in their shoes so that we can see ourselves from their perspective and imagine how we would use what they have to achieve their objectives. The more thought, creativity, and critical thinking that goes into writing our adversaries user stories, the more targeted our defense can be. Time spent here and throughout the entire process is paid back when we go into coding good defense and, hopefully, not being a victim. At a minimum, this user story should identify who, what, when, where, and why. If the situation requires it, we should go further into drawing out a rough plan for their attack so we can develop an even more intimate understanding.

### What do we Defend Against
Now we have developed clear and realistic threat user stories and we need to figure out how we will defend against them. In the previous steps we were considering each entity individually. We now want to build our defense holistically ensuring that we address each threat according to the associated risk level. We can do this by creating a schematic diagram that has each attack point. We can then overlay where we expect each adversary to attack and how they will do it. This will allow us to identify where we should place our resources. We don't want to create two different defenses where one will do, but we also don't want to create one defense when two are really required.

By saying we don't want to develop two defenses where one will do, I'm not saying we should not have redundant defenses. Redundancy is imperative.

### What if we ignore it?
It's not free to do the above process. It consumes staff hours that could be spent coding, talking to clients, and doing other things that have a direct positive incentive. Many of our projects will probably not require such an analysis. Relying on that fact is a risky way to do business. Failing to address threats will have consequences that, when an attack occurs, could cause much more significant loss relative to the time saved by not developing a good defense.
 
### Additional Reading
[Code of Practice for Cyber Security in the Built Environment](https://shop.theiet.org/code-of-practice-for-cyber-security-in-the-built-environment). This is a resource that may be worth purchasing and distributing. I can't preview it, but it certainly seems appropriate. Description: "This Code of Practice explains why and how cyber security should be considered throughout a building's lifecycle and explains good practice, focusing on building-related systems and all connections to the wider cyber environment. The book provides clear practical guidance to help multidisciplinary teams understand how the management of key aspects of cyber security relate to their specific jobs and responsibilities in maintaining the security of a building. Intended to act as an integral part of an organisation's overall management system ensuring the cyber security of building related systems, the Code of Practice will be an invaluable resource for companies looking to improve their building's cyber security."

[10 Steps to Secure Software](https://dzone.com/articles/10-steps-to-secure-software). dzone article that highlights some practical tips to implement to make sure our code is secure.

[Cyber Security Cards](http://securitycards.cs.washington.edu/cards.html). These "playing cards" group considerations into 4 "suits" or categories - Human Impact, Adversary's Motivations, Adversary's Resources, Adversary's Methods - and list nine to thirteen "cards" or items that relate to each suit. 

[Stuxnet](https://en.wikipedia.org/wiki/Stuxnet) - This article highlights two things. First, the incredible capability of a concerted effort. Second, the equally incredible resources that go into mounting such an attack. This brings the final point which is that civilian infrastructure becomes the low hanging fruit. If we expend resources to harden our buildings against natural and man-made hazards, we ought to also give them at least a minimal level of cyber protection. We don't want to read headlines that say a nefarious actor gained access to a specific system through software developed by Arup.

[A Dam, Small and Unsung, Is Caught Up in an Iranian Hacking Case](https://www.nytimes.com/2016/03/26/nyregion/rye-brook-dam-caught-in-computer-hacking-case.html). This article supports the above point that because it takes so much to attack the juicy, but well-defended target the bad actors will expend their resources that get the same the same effect by focusing on defenseless targets.

[Threats, Countermeasures and Attribution of Cyber Attacks on Critical Infrastructures](https://arxiv.org/pdf/1901.03899.pdf). The digital team could end up working on or with many types of critical infrastructure. It is important to recognize the interconnectedness and thus how our products could become the attack surface for something else.

[Deviant Ollam](https://www.sans.org/webcasts/youre-red-teaming-and-im-not-either-108430). Provocative talk given by Deviant Ollam that helps us to think about what red teaming really is and is not.

Jira Issue:[DIG-27](https://arupdigital.atlassian.net/browse/DIG-27)