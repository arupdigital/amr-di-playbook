# The Agile Retrospective as a In-Person Meeting

Jeff Berg

Date: 4.10.2019

## Summary

###
Who should use this document?

This document is meant to be used by a retrospective facilitator to engage an entire team after a significant sum of work has occurred.

#### Why use this document? Why have Retrospectives?
In simplest terms possible, we learn from our mistakes. But we only learn from our mistakes if we admit to them,address them, and change behaviors. In some cases mistakes are out of our control. Tolerance for change and ability to readjust is often an outcome. In other cases, teams might adopt a new Agile methodology or engage new skills on the team to address gaps and anticipate future workloads. 

The example outlined below should take 90 minutes to 2 hours. However larger teams, or teams with more challenges to address may want to consider more time.

#### Guidelines

1. Be Arupian.
2. This is a safe space for open and honest discussion, all contributions are valid.
3. ‘Prime Directive’/baseline for the discussion: Regardless of what we discover, we understand and truly believe that everyone did the best job they could, given what was known at the time, their skills and abilities the resources available, and the situation at hand. 
4. The outcome of a Retrospective is to define executable action items to be implemented in the next phase of work.

#### Variations
This document specifies a Retrospective in the simplest possible form, and was conceived for a team new to Retrospectives. Each team is encouraged to think of the Retrospective process as a way to define executable actions to be implemented in future phases. There are many types of Retrospectives. For example, Zombie, Kite or Pirate Ship retrospectives can be completed. Look for activities which will enourage whole team particpation honesty, and above all else, safety to speak freely.

### What is a Retrospective?

A retrospective is generally associated with Agile, as it&#39;s part of the Agile process. The team comes together to assess what happened during an iteration of software development.

#### The primary goals of the Agile Retrospective

The Agile Retrospective is meant for the team to ask themselves three questions and discuss the answers.

1. What went well?
2. What did not work well for us?
3. What actions can we take to improve out process going forward?

#### The primary goals of the Arup Retrospective

The Arup Retropective starts with the Agile Retrospective but includes a wholistic view that not all project concerns are rooted in coding processes. Business needs often drive and effect code development. The Arup Retropective seeks to ask itself for 8 primary goals and stipulates that a recording method and subsequent sharing to benefit all Arup teams who may look upon the challenges and subsequent improvements discovered during the Retrospective process on any project.

1. What went well?
2. What were the benefits for Arup and for the client?
3. What were the challenges encountered?
4. What were the repercussions for Arup, for the client?
5. What were the primary lessons learned?
6. What actions can we take to improve our process going forward?
7. Utilize the Retrospective activity to allow the data to be captured.
8. Share the results of the activity when possible so that all Arup projects benefit from lessons learned.

### A retrospective is not a solution for troubled projects.

A retrospective is not a post-mortem. A post-mortem is different and has requirements for projects that have not gone well and are no longer functional. A retrospective is for the team, staff and organization to improve processes and teaming.

A retrospective is not an assessment of team happiness, excitement or enthusiasm. If a team has issues with these topics, alternative activities should be used.

### When should I carry out a retrospective?

A retrospective can occur at the end of a Sprint, an Epic, test cycle, or deployment and delivery. A retrospective can be utilized to help a troubled team in the middle of work however and should be seen a framework for open discussion and problem solving.

### What tools do I need to undertake an In-person Retrospective?

- A large conference room space with privacy. The ability to speak freely and debate is key. Some spaces are not good for these activities where they may interrupt others or where sensitive project information should be guarded.
- A large white board or flip-board paper. When possible use a flip board with tear-away sticky back paper that can be affixed to the meeting space wall.
- Post-it notes. Get a few colors. Although colors is not a requirement, team members often naturally use color to distinguish their ideas or subgroup specific topics.
- Trello. An administrator should have a Trello board for recording the contents of the post-it notes during or after the event.

# Activities

## Guiding the group

Welcome all to the Retrospective, and give the retrospective a name, with the dates the retrospective applies to. For example: E Corp Project, Epic 12, January 1, 2019 to March 31, 2019. Remind all participants to avoid blame, be respectful and follow the instructions of the facilitator. Remind all that if they are becoming passionate about a topic they may be asked to move on in the interest of time. Keep the conversation moving. If the facilitator asks the group to move on, the facilitator should add a column on the Trello or Whiteboard, record that need to return to a conversation in an alternative activity such as a one on one meeting, or a brand new meeting with the team which may address a large concern with many details.

## Activity1: Things that went well!

#### Prep:

The facilitator, ahead of the event should login to Trello and copy the Retrospective Trello board, or create a new one with the following headings:

Things that went well, Benefits for Arup, Benefits for Client, Benefits for Team

#### Activity:

**15 Minutes** Team leadership is asked to exit Skype. They will call back in in 15 minutes.

The facilitator reminds participants that this activity is about what went well. If a process or team task went well, but still needed improvement in some areas, those ideas should be kept aside for now.

Team members are free to create columns and add as many cards to the board as they wish for ten minutes. Preferably each card that describes what went well should have matching cards in the other columns. If a card already exists, there is no need to create a new one. Duplicate cards should be deleted when encountered. Participants are encouraged to chat over Skype about duplicates, moving cards, or creating new columns.

**5 Minutes** Voting: The group spends 5 minutes voting. Participants are instructed to open the Trello menu at the top-right of the screen. Then select stickers. They can drag a check mark sticker to cards they feel are important to the success of the project. They encouraged to apply only one sticker to each card. Participants are encouraged to discuss why a particular card is the most important one. 

For a particularly participitory team, votes cane be limited to perhaps 3 or 5 allowable votes from each team member so they focus on exactly what is most important to them.

**5 Minutes** Team leadership returns to the call.

The facilitator sums up the board, noting highly voted topics. The facilitator encourages leadership to comment on the sentiments on the cards.

**5 Minutes**

The team can ask leadership questions about the cards or discuss topics the cards missed.

**5 Minutes**

Leadership can discuss and ask questions of the team, as well as bring up topics not listed on the cards.

## Activity2: Challenges Encountered.

#### Prep:

The facilitator, ahead of the event should login to Trello and copy the Retrospective Trello board, or create a new one with the following headings:

Challenges Encountered, Repercussions for Arup, Repercussions for Client, Repercussions for Team

#### Activity:

**15 Minutes** Team leadership is asked to exit Skype. They will call back in in 15 minutes.

The facilitator reminds participants that this activity is about challenges encountered. If a process or team task went poorly, but has some positive aspect, participants are encouraged to note those in the card comments.

Team members are free to create columns and add as many cards to the board as they wish for ten minutes. Preferably each card that describes challenges encountered should have matching cards in the other columns. If a card already exists, there is no need to create a new one. Duplicate cards should be deleted when encountered. Participants are encouraged to chat over Skype about duplicates, moving cards, or creating new columns.

**5 Minutes** Voting: The group spends 5 minutes voting. Participants are instructed to open the Trello menu at the top-right of the screen. Then select stickers. They can drag a check mark sticker to cards they feel are important to the success of the project. They encouraged to apply only one sticker to each card. Participants are encouraged to discuss why a particular card is the most important one.

**5 Minutes** Team leadership returns to the call.

The facilitator sums up the board, noting highly voted topics. The facilitator encourages leadership to comment on the sentiments on the cards.

**5 Minutes**

The team can ask leadership questions about the cards or discuss topics the cards missed.

**5 Minutes**

Leadership can discuss and ask questions of the team, as well as bring up topics not listed on the cards.

## Activity3: Action Items

#### Prep:

The facilitator, at the end of the previous activity should add a column to the challenges encountered activity board called Improvement ideas. This should be placed next to the Challenges Encountered column.

#### Activity:

**20 Minutes.** Team leadership can stay on in this activity.

The facilitator reminds participants that this activity is about action items. If new ideas about challenges or what went well come up, those should be added to the previous Trello boards.

Team members add improvement idea cards next to each challenge encountered card to the board for ten minutes. Each challenge should have a matching proposed improvement idea card. If no action item exists, an empty card should still be added. Participants are encouraged to chat over Skype about solutions and add comments with supporting ideas to each card.

**5 Minutes**

The facilitator sums up the board, noting interesting opportunities. The facilitator encourages leadership to comment on the sentiments on the cards.

**5 Minutes**

The team adds champions to each card who will take on immediate next steps regarding the improvement idea. The first step can be discussed offline. Champions can be management or team members.

## Activity4: Wrap Up

**5 Minutes Wrap Up**

Everyone is encouraged to chat about any items they felt were missed or set to a lower than expected priority.

**Facilitator:**

The Trello board URLs should then be shared with leadership for capturing important points and directing improvement.