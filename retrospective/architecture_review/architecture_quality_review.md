# Architecture Quality Review

Jeff Berg

Date: 5.3.2019

# Software and Architecture Quality Review

## Intent

This document outlines the process for an architecture review and the subsequent outcomes that can be expected. It can be used as a template for reviewing any software architecture. For large teams, a full retrospective is recommended. The Arup document titled RetrospectiveInPersonMeetingActivities.docx has more information regarding retrospective activities.

## General Challenges Found During Typical Architecture Reviews

- We recognize that in the interim between design and deployment, the technology landscape changes.
- Improvements, services and offerings in the marketplace can change architecture choices.
- Competitive products may appear in the marketplace that may make a custom code development no longer viable.
- Agile methodologies, despite a cyclical, fast-fail process can fail in corporate environments where the use of a particular software solution may have abstract, internalized momentum.

## Outcome

It is not the objective of this review and resulting document to condemn or judge a particular software solution, the resulting code quality or workability of the solution. Where challenges are discovered in review, solutions are offered in a positive and constructive way which drives towards successful solution design.

## Considerations

When reviewing architecture there are primary dimensions in consideration. Each has specific Arup goals and requirements. An assessment should, whenever possible provide the Arup preferred solution and conform to security requirements. Many projects in Arup are research oriented and experimental. In these cases, exceptions can be made for time, budget, and feasibility concerns. However, if any architecture, code or data is found to put Arup or any person in jeopardy in any way, this review process should be stopped, and the issue escalated immediately to the proper level of management and IT staff.

### Dimensions for Review

#### Security

#### Performance

#### Accessibility

#### Data Ethics

#### Foreground and Background IP

#### Maintainability

# Project Summary

This section is to be filled out by the reviewer and considered when forming thoughts and opinions, as well as solutions. Budget often drives feasibility and quality.

### Name

Frew Tilt

### Reviewer

Jeff Berg

### Budget

???

### Current Spend

???

### Project Number

???

### Overall Project Description

Frew Tilt is a tool for embedded retaining wall design. &quot;Fast analysis with high quality and flexible output, Frew checks the stability of cantilever and propped retaining walls and predicts the displacement, shear forces, and bending moments of the wall. The program also calculates the earth and water pressures on each side at each construction stage. Suitable for sheet pile, secant, contiguous or diaphragm walls, it supports the latest building standards. Advanced Program features include seismic analysis, wall relaxation and drained to undrained transition.&quot;

This program traditionally runs independently with single stage analysis in an EXE file for windows.

### Design Intent of Components in Review

In the Frew effort under review, the solution is designed to run in the cloud, relieving the engineer from installing a local program on a machine capable of running the analysis in a timely manner. As well, offering retaining wall analysis as a service would free up the analysis to be included into other software solutions with a broader range of structural analysis capabilities without needing to compile in all the required code for the analysis into the software. The proposed cloud solution also allows for multiple analysis to run in the cloud in parallel, freeing up client machines for multitasking.

## Why is this Software in Review?

The team which developed the current cloud solution has highlighted concerns in the ongoing maintenance and development of the project.

## Team Interview/Retrospective

Challenges were identified during review with the team on May 2, 9am CST. Attendees were Carol Matthews and Lloyd Pickering.

## Challenges Identified During Interview/Retrospective

- No permanent development team exists. The code was moved from coder to coder without thorough completion. Each time the project was transferred, some knowledge was lost and design changes were made.
- Code that is no longer in the project brought the project down a path of development that was ultimately deleted from the codebase. This presented significant challenges in recovering usable code, time, and design thinking efforts.
- The design included the use of the Arup Worker Framework (AWF). This solution was the result of a project called p500 which sought to effectively distribute CPU intensive tasks to workers for parallel processing. Workers in the design sit on a specified number of machines which subscribe to a queue of requests. When a request is found, the first worker that finds the request accepts the job and remove the request from the queue. When the work is done, it notifies the original requester that the work is complete as well as the location of the data results.
  - The Primary challenge with AWF is ongoing support. Rather than specifying a POST request which contains parameters for running analysis, a Frew Tilt specific package must be prepared for AWF. Conversely AWF must be programmed to accept the bespoke Frew Tilt package. This requirement of AWF programming results in the need for a coder with deep knowledge of AWF to make minor changes to the Frew Tilt workflow.
  - An original estimate of 6 weeks has ballooned to 7 months, primarily due to delays in development due to team changes but also due to bespoke AWF development coordination needs.
  - This is not a critical review of AWF which serves specific purposes for its original design intents.

##
Dimensions Review

### Security

At the time of review the minimum viable product requires an Arup ID to access the system and is deployed with Arup IT approval. Productization as an API or Software as a Service requires thorough security analysis due to monetary and DoS risk of parallel computing in the cloud.

### Performance

At the time of review, the application has yet to be deployed for performance and feasibility testing. Initial reactions from the team are favorable but anecdotal.

### Accessibility

At the time of review no accessibility considerations or steps have been taken.

### Data Ethics

At the time of review, no personally identifiable data or financially sensitive project information is sent to, stored or returned from the server, databases, related services or reports. FE mesh data could be construed as structurally sensitive data on some projects and should be considered for review.

### Foreground and Background IP

At this time all IP is internal and owned by Arup. The Frew solution itself was developed by a researcher who is part of the Arup team.

### Maintainability

Due to the AWF framework, maintenance and further development requires specific investment to continue both AWF development and Frew Tilt in parallel.

# Recommendation

At the time of this review, it is recommended that AWF be replaced with a simpler, bespoke solution using AWS Batch computing with Python. The aforementioned challenges of ongoing development and support of parallel development teams makes the project monetarily and organizationally unfavorable for performant code, return on investment and highspeed evolution of the product.

## Pros

- Single development team
- Single code base
- No need for a worker distribution framework software package
- Ease of porting existing DJANGO interface to AWS Batch architecture
- Ease of running the code locally with simplified AWS development environment
- Define the runtime environment image for the worker in Docker
- Store the image on Amazon
- Secure the work with an Amazon IAM role
- Submit and run jobs without worrying about specific instance creation, AWS Batch handles provisioning
- Per-second billing
- The ability of workers to instantiate secondary workers for complex, multi-tiered tasks
- Configure failure mechanisms in AWS rather than complex code.
- Lambda serverless interface to AWS Batch simplifies Frew as a service.

## Cons

- Requires AWS and AWS specific Batch product integration
- Migrating to another cloud solution provider in the future may be difficult
- Loss of AWF effort and cooperation
- Modification of job definition and data package
- Modification of completed job notification handler
- Modification of data parameter packaging to AWS Job Definition parameters

## Next Steps

Provision a code developer (Recommend Andy Huang in NYC) to develop a proof of concept of AWS batch capable of running a Frew Tilt analysis in a hard-coded, single use proof. **This proof will be used to estimate the actual AWF replacement with AWS Batch.** The following tasks can be completed in 40 hours with a hello world, with a recommendation of a follow-on 40 hours to work with the Frew team on Frew worker integration. Determine if AWS Batch offers advantages in development of Frew without AWF dependence.

- Docker definition
- Instance definition, vCPU compute resource min, max, desired
- Job Queue definition
- Lambda Integration
- Environment definition
- Python Coding in Code Branch
- IAM setup
- Manual Batch configuration (replaced by cloudfront in production)

# Conclusion

- Continue user testing with the current Frew Tilt AWF integration.
- Work with AWS Batch to prove independent development of Frew.
- Evaluate AWS Batch with a proof of concept to determine if suitable for AWF replacement and cost comparison between Batch and AWF in ongoing development and maintenance.