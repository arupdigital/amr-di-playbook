# Project Rationale
This document outlines what to include in a project rationale, the first step in the discovery phase of the experience or product creation process. Considerations include identifying reasons for undertaking a project, crafting a concise problem statement and evaluating competing or existing technologies in the problem space.

Jira Issue:[DIG-95](https://arupdigital.atlassian.net/browse/DIG-95)