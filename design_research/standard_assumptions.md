# Standard Assumptions

Danbi has a great list of assumptions that should go alongside any MTA scope of work proposal.  Those assumptions would make a good baseline for use on any project where we are making a client-facing proposal.  I suggest Danbi put these assumptions up so others can use and add.

Jira Issue:[DIG-96](https://arupdigital.atlassian.net/browse/DIG-96)
