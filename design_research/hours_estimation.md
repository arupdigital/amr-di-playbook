# Hours Estimation

Author: Jinsol Kim/Zak Kostura 

Last Modified: 2019-06-18

## Introduction

### Who should use this document?
Digital Insight team members responsible for developing or contributing to an estimate on a project or opportunity.  That estimate could ultimately be an hours estimate or a fee estimate.

### Why use this document?
The estimation of time (and thus cost) associated with the delivery of a digital product or service is an activity that sits at the nexus of the cultural collision between conventional Arup disciplines (who seek detailed project definition up front) and software developers (who understand that the digital product roadmap is fluid and unpredictable).  Project managers and ultimate end clients often expect some form of document that formalizes and caps the financial expenditures associated with a project, and as a result it is critical that the Digital Insight team have a robust strategy for bounding project investment while maximizing opportunity to deliver an effective and valuable end product.

This document outlines strategies and methods for estimating hours associated with the delivery of digital scope on Arup projects.

## What should be included in an estimate
The aim of the estimator should be to account for all tasks involved in the specific phases of the product life cycle that are to be included in that estimate.  Generally, a requested estimate is intended to cover all work leading up to a specific deployment milestone. 

### Be explicit about your assumptions
In addition to details on tasks and itemized hour estimates, a robust estimate should include critical assumptions that the estimator had to make in order to arrive at the estimate.  These assumptions could be included within the estimate document itself or as an attachment to the estimate.  If an attachment is provided, the estimator should take care to ensure that the estimate itself can't be easily spearated from the assumptions.

### Frame the phases of work that will be covered
The scope of work captured in an estimate could be bounded by a phase in the life of the digital product, such as the deployment of a stable version of a digital tool.  It could also be bounded by an external milestone, such as the completion of construction on a rail project or simply the end of the financial year, if it is an internal tool funded by central budgets.  The estimator should be clear about the assumed beginning and end before preparing the estimate, and should be sure to include this in the stated assumptions.

Regardless of the type of bounding milestone, there is risk associated with timing.  Lingering bugs and feature requests could delay the declaration of a product-based milestone such as a "stable version".  Delays to the broader project could lead to unforeseen extensions to the development and/or maintenance phases.  If such risks could have a substantial impact on costs, the estimator should include assumptions to help mitigate these risks.

### Provide a robust estimate for a quality product
Estimators should be sure that their estimates account for the tasks and events that are necessary to design and build a good product.  These include:

#### Discovery phase work
It's important to consider early work needed to develop a vision statement and business strategy, interview users and research the external market to understand the ecosystem of existing products.

*Jinsol do you have any specific guidance on how to include this item into your estimates?*

#### Code reviews
Code reviews are an essential part of quality assurance.  This should be factored into estimates, either as an explicit line item or a factored increase in development time for features.

*Jinsol do you have any specific guidance on how to include this item into your estimates?*

#### Testing and User Feedback
The estimate should include hours for establishing a testing plan, properly setting up staging sites, engaging testing participants, and documenting user feedback if external users are part of this phase.  

*Jinsol do you have any specific guidance on how to include this item into your estimates?*

#### Documentation
Projects should be properly documented, generally in readme files embedded in the project stack.  Allocate time for proper documentation.

*Jinsol do you have any specific guidance on how to include this item into your estimates?*

#### Retrospectives
This should be considered an essential part of a project, as they help identify technical debt that should be inventoried at all phases.  Even at the end of a development project, retrospectives should be used to uncover and document liabilities in the delivered product that should be understood internally for the purpose of ongoing maintenance and future feature development or refactoring.  Note that technical debt identified in a final product delivered to an external party should be handled as sensitive information, and used by the team to validate the sufficiency of that deliverable.

*Jinsol do you have any specific guidance on how to include this item into your estimates?*

#### Roll-Out
The proper deployment of the tool may involve the development of promotional or training material, or the engagement of users for the purpose of familiarizing them with the product.  If this is to be included in the scope of the estimate, make sure it's included.  If not, consider mentioning this in the assumptions.

## Always mention maintenance
Ongoing maintenance of a digital product may or may not be included in the scope of the requested estimate.  If it is, ensure it is framed in a way that enables the costs to be scaled to longer or shorter product lifetimes.  Also include any provisions necessary for handoff to other maintenance teams if appropriate, or for the development of ticketing or other formal tracking systems.

## Mitigate technical debt
Lean estimates may be attractive to potential clients, but tight budgets can easily lead to the accrual of technical debt that undermines the robustness of the code base and renders future feature development time consuming and expensive.  It is left to each estimator how to factor this in, but the following guidance is given by Camille Fournier in her book "The Manager's Path":

> Dedicate 20% of your team’s schedule to “sustaining engineering.” This means allowing time for refactoring, fixing outstanding bugs, improving engineering processes, doing minor cleanup, and providing ongoing support. Take this into account in every planning session. Unfortunately, 20% is not enough to do big projects, so additional planning will be needed to get major technical rewrites or other big technical improvements. But without that 20% time, there will be negative consequences with missed delivery goals and unplanned and unpleasant cleanup work.

## Estimate reviews
The scale of financial investment addressed by an estimate can vary widely.  The following requirements apply to estimates, depending on the total cost conveyed:

### Estimates totaling less than $20k
No requirements for review apply, however the estimator is encouraged to evaluate the context of the proposed project and establish if a review by other members of the Digital Insight team is warranted.

### Estimates totaling between $20k and $50k
In addition to an estimate, the estimator must develop (or work with someone to develop) a vision statement that succinctly captures the overarching goal of the product to be developed.

The scope and assumptions of the estimate must be reviewed by a line manager before passing on to individuals outside of the Digital Insight team.  The review is intended to assess the scope of the estimate against the vision statement and confirm the suitability of the documents provided.

### Estimates over $50k
Estimates in this price range are subject to the requirements of the aforementioned bracket.  In addition to the vision statement, estimators should facilitate the development of a system diagram to illustrate the anatomy of the digital product assumed in the estimate.  If the estimator and line manager jointly conclude that the scope of the estimate is aligned with the vision statement, the scope should be provided to the regional Digital Skills Network Leader, who will perform a software architecture review.  The software architecture review is intended to identify any opportunities to simplify the system diagram.

## Examples

* Jinsol - is it possible to attach or refer to some examples of estimates?

