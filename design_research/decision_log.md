# Decision Log
This documents the methodology for keeping a design decision log. this log tracks why design choices were made and often goes hand in hand with meeting notes during the solution design process.

Jira Issue:[DIG-16](https://arupdigital.atlassian.net/browse/DIG-16)