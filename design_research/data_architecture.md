# Data Architecture
This document goes into details about data architecture concerns.

Jira Issue:[DIG-10](https://arupdigital.atlassian.net/browse/DIG-10)

---

## Gereral considerations

1. the nature and the complexity of data
    - structured vs unstructured
    - continous vs discrete
    - ingestion rate
    - schema change and the speed at which it is changing
    - volume

2. describing load
    - reads/writes ratio
        - write once and read many times?
        - if update, override or append with version number?
        - read and write unit size?
          
    - read
        - requests/second, number of simutaneously active users, etc.
            - using cache? how to invalidate and update value to guarantee data consistency?
        - data consumers geo locations
            - data replica? what is synchronization strategy?
        - data access pattern
            - random access by some key?
            - range scan?
            - roll-up and drill-down on dimemnsions?
            - text search?
            - which data is "hot" or "cold"? access pattern change because data is "hot" or "cold"?

    - write
        - requests/second, number of simutaneously active users, etc.
            - using queue? what is the transaction control strategy and what is the data consistency guarantee?
        - streaming? where and when in the system to do batch processing? 
        
    - upstream data sources
        - push?
            - rate?
        - pull?
            - data update rate of upstream sources
            - rate limit? if hit rate limit, how to stop and resume?
        - authorization method

    - growth
        - how is system affected if increasing the load with resources unchanged? or when increasing the load, how much resources do we need to increase to keep the performance unchanged?

   
3. performance requirement
    - what is the the number of records we can process per second, or how long does it take to run a job?
    - what is the service's reponse time? What are the percentiles?

    - tolerating faults
    - prevention faults with security matters
    - allow processes to crash and restart

4. data ethics
    - do we need this data? if no then don't store it or delete it.
    - generating synthetic data for testing models if possible
    - is the data really anonymous? if certain identity data is necessary, consider random or consistent replacement. for example instead of storing certain age, replacing with age range if it makes sense.
    - is there any encryption put in place?




## Things to consider by Data Type

### Structured data

The goal of this part is not to teach RDBMS (relational database management system). RDBMS is so dominant that all progammers should understand at least the basics of RDBMSs (sunch as the concept of nomalization, relations and joins, SQL, basic DML and DDL commands of at least one mainstream RDBMS, etc.)

A thorough introduction of RDBMS: http://infolab.stanford.edu/~ullman/fcdb.html (PART I and PART II)

These are some of the things that need extra attention during schema design and writing application code:

1. Enforcing referential integrity (https://en.wikipedia.org/wiki/Referential_integrity)
   
    Referential integrity refers to the accuracy and consistency of data within a relationship, it requires that if a value of one attribute (column) of a relation (table) references a value of another attribute (either in the same or a different relation), then the referenced value must exist.

    - A table (called the referencing table) can refer to a column (or a group of columns) in another table (the referenced table) by using a foreign key. The referenced column(s) in the referenced table must be under a unique constraint, such as a primary key.
    - On inserting a new row into the referencing table, RDBMS checks if the entered key value exists in the referenced table. If not, no insert is possible. 
    - Specify actions on UPDATE and DELETE, such as CASCADE (forwards a change/delete in the referenced table to the referencing tables), NO ACTION (if the specific row is referenced, changing the key is not allowed) or SET NULL / SET DEFAULT (a changed/deleted key in the referenced table results in setting the referencing values to NULL or to the DEFAULT value if one is specified).

2. Normalization vs Denormalization

    - It's a rule of thumb to normalize data that is updated frequently, and denormalize data that is mostly read.
    - Normalizing is generally a good idea. It reduces redundant information and reduces the likelihood that you’ll have data integrity issues that result from having the same data in different corners of your database.
    - Denormalization should be considered when you're going to require queries using self-joins.
    - Put other controls in place, such as constraints, to make sure information stays consistent if denormalizing.

3. Transaction control

    A transaction is a collection of actions that potentially modify two or more entities.

    The simplest way to implement transactions is to use the features supplied by the database. Transactions can be started, attempted, then committed or aborted via SQL code. Most object-relational mapper libraries support basic transactional functionality as well.

4. Performance



### Documents

Document databases target use cases where data are in self-contained documents; and relationships between one document and another are rare. 
Schema of Document databases (and other no-sql databases) is not enforced on write but assumed on read, meaning application code will take care data integrity and consistency. 


### Time Series

One of the big challenges in handling time series data is partitioning. Time Series data often means range scans, and keeping keys (timestamp alone or concatenated with other column) in sorted order within each partition will make range scans faster. But the downside of this implementation is certian access patterns can lead to hot spots.

For example, all the data generated from a sensor will end up writing to one partition (the one for today) if the partition is by timestamp or timestamp-{other columns}.
To Avoid this, if there are many sensors active at the same time, we could prefix timestamp with sensor name or id. Queries over time and/or sensors will need to be adapted accordingly.


### Full-text search


### Images and Videos
