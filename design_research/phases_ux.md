# Phases of UX
This document addresses the user experience design process for software in Arup Americas.

## Why we do User Experience Design
Everything we design, whether or not it has a user interface, has a user. We can't build good software for real people without including those people in the design process. In order to build useful, delightful products and solutions, we need to understand who our users are, how they behave, what motivates them, and what they want to accomplish. User experience design is the process through which we approach that problem. This process should answer key questions around interaction design, usability, accessibilty, general look and feel, and user flow. 

User experience design is a collaborative process between users, designers, developers, and stakeholders to define a product in terms of the _users_. User experience design isn't concerned with the particulars of the product design, but rather how users experience the product. The insight from the UX design process informs the application architecture, distribution, features, and UI design. 

## What UX Isn't
User experience design does not result in documents that define a user interface or can be taken into development. UX documentation is valuable for development, but does not define any technical requirements or design. Those documents should be generated as part of the UI design process and from the solution architecture. 

UX design is a _process_, not a deliverable. Some UX documents are, however, presented to the client as interim deliverables to get feedback and show progress. 

## UX Design Process
The UX design process should adapt to the specific problem, but in general, there are a few key phases of UX design. 

### Research
This phase should focus on the users, and specifically what are the users' _problems_? This should relate to the brand and its own goals. This phase of research should focus on understanding the problem at hand. A UX designer should go into the rest of the process with a strong grasp of the environment around the problem.

#### Key Questions
- Who are my users?
- What world do my users live in? What factors affect them?
- What is the context around this problem?

### User Interviews
Throughout the process, designers should talk to the users! At first, it's good to limit focus to 3-5 users in the target audience to manage feedback and get a clear idea of the pain points that users have. These interviews are either one-on-one or small focus groups (3-5 people). Designers can also use surveys to get more robust data, but should be careful to design the survey to avoid confirmation bias. 

#### Key Questions
- What do my users care about?
- What are my users' current workflows?
- What pain are users experiencing?

### Analyze
Building off of the research to this point, designers should create user personas that define motivations, goals, and frustrations of the target users. It's useful to create user journey maps (or workflow maps) to help identify pain points and evaluate the process. These user personas can be useful deliverables to the client to communicate the intent of the application to the client.

#### Key Questions
- What personas represent my target users?
	* What is their background, age, gender, location?
	* What are their goals, frustratoins, habits, and needs?

### Wireframes
At this point, we should have enough research to start designing solutions. We're not looking for pixel-perfect designs - instead we make low fidelity mockups of the product. These mockups should convey the overall description and direction of UI and user journeys. These aren't enough to go into actual development, but should give users and developers a clear idea of what's being built. These wireframes are often used as interim deliverables to a client to get feedback in the next phase.

#### Key Questions
- What design best addresses the needs of my users?

### Iterate
As wireframes are being made, designers should get continuous input from both users and developers. Users will help drive the product direction while developers will inform what's possible from a technical standpoint. It is especially important to involve developers in each phase of design as they often can point out potential problems and understand the technology to suggest other solutions. We iterate until we reach a solution that is both feasible and meets all of the needs of the users. As we go through this process, we may find that this could mean redefining what the primary problem is or what the solution should be. We should have these discussions early to avoid developing something that doesn't properly solve the root issues. 

By the time we start writing a line of code, we should have a strong definition of what the _Minimum Viable Product_ is for the users and stakeholders and we should have a strong plan of how to achieve that design.

#### Key Questions
- What is our minimum viable product?
- Does this adequately solve the problem at hand? Did users give feedback? Did the client?
- Is this design feasible? Did developers give feedback? 
- What information are we presenting to users? How is it presented?
- What is the application flow? What's the overall structure?
- What is the visual design of the application?
	* What colors are we using? What font?
	* Should we have a sidenav or tabs? 
	* Where does this button go?
	* What happens if I try to...
	* Is this design mobile-ready?
- How does this product make a user feel?

### Develop
This is where we actually develop a product. At this point, there should be strong agreement among users, stakeholders, designers, and developers what the root problems are and what the product will be. At this point, we could have full UI specification, but UI design work falls under this phase, as well as the dirty work of actually coding the thing. Developers should seek guidance and feedback from the designers as questions inevitably arise. The development process should follow best practices as described in the Playbook.

As we develop applications, we often find technical roadblocks that challenge our initial design decisions. Sometimes, it's better to consider designing different solutions than to try to build workarounds in code. This is not to say that ease of coding trumps design, but we should be aware of how design decisions can affect the development process. Designing differently can often be less time and effort than coding our way through certain problem, and can result in better, more maintainable products. User experience should still be a priority as these decisions are being made. 

#### Key Questions
- What technology are we using?
- Is this problem a technical challenge or a design challenge? Or both?

### Iterate Some More
At development milestones, it's important to get feedback from users and stakeholders. This feedback should center around _usability_, including watching users actually _use_ the product and observing how they respond. 

It's important as we iterate design to avoid scope creep. As users actually use a product, it's natural that they have ideas and new insights, but these should be captured and managed in a way such that the insight is productive. Some feedback will change the initial design, as it should, but we should keep the overall scope to the MVP we defined.

#### Key Questions
- Is the product intuitive for a typical user? 
- Does the user enjoy using the product?
- Can the user accomplish what they need to?
- Are there unexpected new pain points?

### Launch
Once the user testers are satisfied, it's time to launch the product. This can be a single rollout or staged rollout to beta testers for even more feedback at scale. At this point, it's still important to get feedback from users to drive future features. It's useful to include some analytics (such as Google Analytics) to keep track of actual usage. 

#### Key Questions
- Which bar are we going to?

THEN

- How are users responding?
- How does the product perform at scale?

### Keep Iterating
Future phases of development all follow this same process from the beginning, starting with research and user interviews. As technology changes, it's also important to consider if and how new technologies and practices relate to the product. 

#### Key Questions
- What new features do we want to focus on for this round?
- What have users been saying? What do they like? What don't they like?
- What needs to be fixed?
- What needs to be tweaked?
- Are there new technologies we need to be aware of?

### Maintain
Every application requires maintenance, and that work should be included in the lifecycle of the project. This maintenance also has UX ramifications. Applications shouldn't break as time goes on, services should stay operational, bugs should be fixed - users should continue to have good experiences even after development ends. The user experience only ends when there are no more users, so we should consider how the lifecycle of the application affects users.

#### Key Questions
- What's the lifespan of this application?
- Who is responsible for maintenance?
- How will we support maintenance as the application ages?
- How do users report and track bugs? How do developers respond?
- Is there an update cycle? How often should maintenance occur?


## Resources
- [Invision UX Design Process](https://www.invisionapp.com/inside-design/6-stages-ux-process/?itm_campaign=default&itm_source=homefeatured&itm_medium=website&itm_content=default})
- [UX Definition](https://en.wikipedia.org/wiki/User_experience_design)
