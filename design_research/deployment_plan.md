# Deployment Plan
The deployment plan lays out deployment specific plans for testing, deployment and continuous integration. This plan should list and link to concerns such as security and test review best practices in the playbook.

Jira Issue: [DIG-23](https://arupdigital.atlassian.net/browse/DIG-23)