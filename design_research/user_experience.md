# User Experience Design
This document summarizes the approach to user experience design. Other tasks, such as User Interviews go into more detail. It may be helpful to link to those as well as content on moodle and across Arup.

## User Experience Philosophy
### Why Focus on User Experience?
The term *User Experience Design* is misleading because it implies that it is somehow separate from the design process. Whatever design process you use, it will ultimately become the user's experience - regardless of whether you designed for it.

With that in mind, user experience design ought to be at the forefront of the team’s collective mind. It is embedded in the first principle of the [*Manifesto for Agile Software Development*](https://agilemanifesto.org/): “Our highest priority is to satisfy the customer through early and continuous delivery of valuable software.” User Experience Design has plenty of direct and indirect ties to all the other [principles](https://agilemanifesto.org/principles.html) as well. In a way, it could be argued that the entire agile ideology and project management/delivery methods are extensions of recognizing that user experience is paramount.

There are many great benefits to focusing on user experience as our center of gravity during the product development process:
* We continually engage the customer to make sure we are meeting their expectations and needs and view things from their perspective
* We approach problems with a principles first attitude. This requires that we understand the problems well which, in turn, requires that we do quality [user interviews](design_research/user_interviews.md) and write accuracte and all-encompassing  [user personas](design_research/user_story.md)
* We prioritize work that has an impact on the user
* We increase our agility to shift with changing requirements because we become less attached to specific solutions and more focused on solving the right problem with the right solution

In a world where everything is possible and mountains of information are within reach, projects can quickly get derailed because it can be difficult to discern the difference between worthy innovation and shiny, useless distraction. These injects can be from our own ideas, something the client saw, something a competitor developed, or any number of other sources. As self organizing teams pull issues down for development, they need a way to quickly determine whether the task supports the overall goal of the project. This means they need to keep all the users in mind as well as the ultimate goals of the project. To enable low-level decision-making, all team members need to be on the same page.

### Vision, Strategy, and Tactics
Roman Pichler formalizes how to maintain your team’s focus in his 2016 book [*Strategize*](https://www.amazon.com/Strategize-Product-Strategy-Roadmap-Practices-ebook/dp/B01F749SF6/ref=sr_1_1?gclid=EAIaIQobChMIkM2-p_fu4wIViZ6fCh0XJQ68EAAYASAAEgLk_fD_BwE&hvadid=241661029403&hvdev=c&hvlocphy=9067609&hvnetw=g&hvpos=1t1&hvqmt=e&hvrand=5229943690366200935&hvtargid=kwd-260878951835&hydadcr=21904_10171077&keywords=strategize+roman+pichler&qid=1565118113&s=gateway&sr=8-1). In a “why?”-first process that would be as well supported by Sun Tzu, Clausewitz, or Simon Sinek; he suggests that you approach your designs by describing your vision, laying a strategic path to achieve it, and then identify the tactics, tools, and methods that will be used along the way. The assumed first step is that you have a well-developed vision.

| Level | Description |	Sample Artifacts |
| --- | --- | --- |
| Vision | Describes the positive change the product should bring about, and answers why the product should exist. Guides the strategy. | Vision statement or slogan. |
| Strategy | States the path for attaining the vision; captures how the vision should be realized; directs the tactics. | Product strategy, product roadmap, business model. |
| Tactics | Describes the steps along the way, and the details required to develop a successful product. May lead to strategy changes. | Product backlog, epics, user stories, story maps, scenarios, interaction and workflow diagrams, design sketches, mock-ups, architecture model. |
[Pichler, 2016](https://www.amazon.com/Strategize-Product-Strategy-Roadmap-Practices-ebook/dp/B01F749SF6/ref=sr_1_1?gclid=EAIaIQobChMIkM2-p_fu4wIViZ6fCh0XJQ68EAAYASAAEgLk_fD_BwE&hvadid=241661029403&hvdev=c&hvlocphy=9067609&hvnetw=g&hvpos=1t1&hvqmt=e&hvrand=5229943690366200935&hvtargid=kwd-260878951835&hydadcr=21904_10171077&keywords=strategize+roman+pichler&qid=1565118113&s=gateway&sr=8-1)

#### Vision
Developing a vision that is appropriate to the problem at hand is a blend of art and science. It requires those involved in creating the vision to have enough experience to identify the right problem and enough technical competence to leverage the capabilities of technology without exceeding them. It will be a tough row to hoe for all involved if the raison-d’être is not a compelling one or if the vision is not a reasonably attainable one. After a process that can be as iterative and collaborative as the complexity of the problem demands, the team can formally state its vision. As with many leadership challenges, leaders must strike the balance of developing a good vision statement without expending too many resources or accidentally sliding into development.

This vision statement should succinctly state what the product will do and why. The statement’s simplicity will allow it to be applied as a litmus test to untold numbers of questions that arise throughout the product development. It must be broad enough that adjustments to the program don’t mandate a complete re-write. We want to encourage flexibility and adaptability in the process, so we avoid rigid and overly prescriptive statements. As we launch new phases, new features, and have professional disagreements about how to proceed, the vision statement will reinforce the center of gravity and user experience over and over again.

#### Strategy
With a good vision, the strategy and tactics flow a little easier. If developing strategy and tactics is very difficult it may be worth revisiting your vision. Strategy is inseparable from vision. According to Pichler, strategy “states the path for attaining the vision; captures how the vision should be realized; directs tactics.” To capture how we attain the vision, we need not only know where we are going, but also where we are and in a broad sense the major milestones between here and there. These milestones can be major releases, specific hurdles overcome, or standard process completion markers. The strategy is not just an enterprise’s product development process regurgitated, but rather it should probably look more like that process applied to the problem at hand. Like vision, strategy is useful for every single person involved because it creates a shared understanding about what we are doing, why we’re doing it, and how we’re going to get there. Informing the entire chain is crucial to enabling initiative that pulls in the same direction as everybody else’s efforts.

#### Tactics
Lastly, is tactics. Tactics are what is used to make the incremental gains that ultimately achieve what the strategy laid out. It is the day to day project management that encompasses everything from task assignment, style guides, code reviews, and everything in between. It also addresses the specific tools and methods in terms of languages, architecture, databases etc. that are used to develop the project. Arguably, tactics should be the most consistent thing from project to project and within an organization. While we always want to acquire and test new tactics that we can apply when the right scenario presents itself, we should hesitate before attempting things we’ve never done in ways we’ve never tried. As an example, this entire playbook is a codification of the Americas Digital Insights team’s tactics. It has been built in a way that it is a living document and updated with time. This allows the team to evolve and grow rather than require a revolution or risk extinction.

An aside on tactics and teams: teams that work together often have optimized their roles, tactics, and methods of communication. This happens from the “storming, forming, norming, performing” phases of team development. With optimization comes a resistance to change. Again, the challenge is to the leaders to foster a learning culture and identify the difference between an optimized and stagnated team. Leaders must also understand the strengths and weaknesses of their team’s tactics so that they can leverage them to best achieve their strategy.

### User Experience and Vision in Sweet Harmony
It is abundantly clear that developing and describing a good vision has pervasive good or bad effects for the entire product development. Change a few words at the beginning and you will likely end with a completely different product. It is also abundantly clear that whether the product is worth the investment can largely be measured by how good the user experience is. At all costs, we cannot allow the vision to be at odds with the user experience or vice-versa. They must be one and the same from the beginning. This will synchronize teams, align priorities, and motivate the team members to seize the initiative. How do we make sure the vision encapsulates user experience? Read the vision statement. If you don't have a macro level feel for the user experience, then you probably need to go back to the white board and: 
* Write a short narrative that outlines the user experience
* Write a short narrative that outlines your vision
* Try to write another vision statement in which the user experience and vision are mutually supportive
* Repeat until your vision statement supports your user experience
* Try to adapt the vision to the user experience, not the other way around

## Further Reading
Below are links to great resources that explain the concepts and fundamental processes to successful user experience design.

[User Experience Design Process](https://uxplanet.org/user-experience-design-process-d91df1a45916) – Saadia Minhas – Apr 23, 2018. 
Proposes a straight forward process that lists the major steps and sub-steps of user experience design with clear inputs, processes, and outputs. Unless somebody has something more academic or specific to our organization to write, I would recommend wholesale adoption of the article as Digital Insight’s User Experience Process.

[Why You Need UI Guidelines?](https://uxplanet.org/why-you-need-ui-guidelines-d380e407b759) – Saadia Minhas – April 9, 2018. 
Highlights how easily the style of your work can become inconsistent and messy and makes recommendations about ways to help combat that.

[User Experience is the Most Important Metric You Aren’t Measuring](https://www.entrepreneur.com/article/309161) – Michael Georgiou – March 1, 2018. 
This article emphasizes the importance of the above two articles and why user experience matters. Although geared more towards enterprise sites that directly generate revenue, the applications we develop should strive to attain the same high level of user experience.

[Investigating and promoting UX practice in industry: An experimental study](https://www.sciencedirect.com/science/article/pii/S1071581913001390). This special issue focuses on “Interplay between User Experience Evaluation and Software Development”, stating that the gap between human-computer interaction and software engineering with regard to usability has somewhat been narrowed.

[The UX Book](https://www.sciencedirect.com/book/9780128053423/the-ux-book). The discipline of user experience (UX) design has matured into a confident practice and this edition reflects, and in some areas accelerates, that evolution. Technically this is the second edition of The UX Book, but so much of it is new, it is more like a sequel.

[Introducing *Letting Go of the Words*](https://www.sciencedirect.com/science/article/pii/B9780123859303000327) - Lots has happened in the digital world since the first edition of Letting Go of the Words. In this new edition, you'll find tips and examples for content strategy, mobile, search engine optimization, and social media.

## Moodle/Lynda Courses
Could not find anything useful on Moodle. The below Lynda.com courses would need to be requested through the digital champion.

[UX Research: Going Guerilla](https://www.lynda.com/Sketch-tutorials/UX-Research-Going-Guerrila/797733-2.html). Lynda.com course that would need to be added to Moodle. Description: "What if you lack the time or resources to devote to full-blown UX research? In this course, join Amanda Stockwell as she dives into an efficient UX approach that's perfect for such a situation: guerilla research."

[UX Foundations: Prototyping](https://www.lynda.com/InVision-tutorials/UX-Foundations-Prototyping/605436-2.html). Lynda.com course that would need to be added to Moodle. Description: "Protoyping allows designers to quickly and inexpensively explore multiple iterations of designs. Protoypes can be submitted for testing and feedback, leading to better experiences for the business and for users."

[UX Foundations: Style Guides and Design Systems](https://www.lynda.com/Web-tutorials/UX-Foundations-Style-Guides-Design-Systems/669550-2.html). Lynda.com course that would need to be added to Moodle. Description: "A style guide is the document that sets the tone for how your organization communicates and how it designs its products...Join UX specialist Chris NOdder, as he leads you through building  a style guide that describes your UX strategy in a unified way that everyone can understand."

Authored by [Daniel Moore](daniel.l.moore@pm.me)

Jira Issue:[DIG-9](https://arupdigital.atlassian.net/browse/DIG-9)