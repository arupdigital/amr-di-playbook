# Project Documentation Templates
This document should list all project documentation templates needed for the team. It should then link to those templates. if a template does not exist a new jira task should be created for the document template to be created. Where possible, a template should be created from existing project documentation to save time in developing templates.

Jira Issue:[DIG-22](https://arupdigital.atlassian.net/browse/DIG-22)