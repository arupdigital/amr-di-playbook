# Due Diligence
This is a checklist of activities designed to minimize risk. It will contain concerns rangin from a market analysis, go-no go for code development, security risk, user testing, data ethics, bias, etc.

Jira Issue: [DIG-24](https://arupdigital.atlassian.net/browse/DIG-24)