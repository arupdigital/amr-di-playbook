# Introduction

### Who should use this document? 

This document is intended to serve as a useful reference to anyone, including Arup staff who have or may someday have a relationship with the Digital insight team, or external collaborators who work with the Digital Insight team to deliver projects. 

### Why use this document? 

The role of a digital team in a consultancy requires definition, to serve as a foundation for productive strategy development, planning and collaborating.  This document should be used as a reference for understanding who we are, our core mission, how we work to deliver value to the firm and clients, where we fit into the digital product lifecycle, and our skill sets. 

## What is Digital insight? 

### Core Mission 

Digital transformation in the Americas Region of Arup is championed by the digital insight team.  The team operates cohesively across offices and collaborates with a wide range of Arup teams in buildings, infrastructure, consulting and business services.  

Put succinctly, the mission of digital insight is:  

> Digital Insight helps external clients and internal Arup teams transform what they do.  Our data services and software tools help people to be more effective, productive and creative. 

Given the maturity of the digital product development industry, the Arup digital insight team emphasizes as its core competency the delivery of services that build upon our rich domain expertise in the built environment. Our products do not seek to replace our core services, but rather repackage, streamline or enhance their value to our clients. 

### How We Deliver Value  

To do this, the digital insight team typically works on three types of projects: 

1. On internal projects, generally funded on central budgets, aimed at transforming or expanding our conventional services offerings through automation, software deployment, or data analytics 
2. On client-facing projects whose primary scope involves the delivery of conventional services.  On these projects, the insight team provides digital services that enhance the way we deliver conventional work or value-adding, direct-to-client digital services 
3. On client-facing projects whose primary scope involves the delivery of digital services 

In addition to core digital services offerings, the digital insight team acts as a conduit between Americas Region staff and best-practice perspectives and techniques sourced from other regions of Arup and the industry at large.  We engage in initiatives to catalyze the adoption of emerging technologies, workflows and methods by all Arup staff. These initiatives include: 

* Project digital inception events 
* Skills-oriented groups 
* Regional training sessions 
* Hackathons 

### Our Role in the Digital Product Lifecycle 

The digital insight team is typically active throughout all phases of the digital product lifecycles, including: 

* business analysis/market research 
* user experience design 
* user interface design 
* software development 
* user testing/QA 
* deployment and roll-out 
* maintenance  

To accomplish this, the digital insight team collaborates with other Arup teams, principal among them: 

* Foresight & Design Strategies 
* Advanced Technologies and Research 
* Information Technology & Communications 

### Our Skill Sets  

This network collectively possesses a broad understanding of emerging methods and technologies, including product design, agile development, artificial intelligence, geospatial data visualization, IoT integration, mobile data capture and cloud deployment.  

## Harmonizing with the Culture of a Consulting Firm

Arup is and will continue to be principally a consulting firm operating in the built environment.  Since its inception, the firm has nurtured a strong culture of delivering exceptional service to a wide range of clients in a number of forms ranging from traditional single-discipline design, to integrated multidisciplinary design, to consulting and advisory services on a wide range of technical and socioeconomic topics.  As the digital product development arm of this company, it is our responsibility to set out on a path of collaboration that is at once informed by the best practices of lean software development and the well-established methods of finding, winning and delivering our conventional work.

In numerous ways, there is a clash among these cultures.  Modern software development is often stymied by the constraints imposed by thoroughly defined scope, fees and timelines at the start of a project.  In contrast, our conventional projects often break down if we fail to establish these parameters early.  Continual harmonization of these cultures is an important driver behind the mission of the Digital Insight team, and is the fundamental reason for a home-grown playbook on our specific perspectives, philosophies and methods.  To succeed at our work, it is essential that the Digital Insight team embodies staff that are at once exceptional at delivering technological solutions and empathetic with the needs and ways of working embodied by everyone else at Arup.  In a very real way, they are our client base.

To do this, we reframe generic software development methodologies such as Agile to focus on projects rather than products (when appropriate).  We understand that the unavoidable fixed budgets and timelines mean that we must continually revisit our vision statements, key features, and development backlogs and remain flexible about the end result.  Like all digital product development firms, we must remember that the software we create is a means to an end rather than the end itself: our work delivers better buildings, bridges, rail lines, airports, schools, cities and more.  We are successful if the digital transformation we catalyze has a positive and measurable impact on Arup's overall role in transforming the built environment.